[![Java version](https://img.shields.io/badge/Java-19-blue)](https://img.shields.io/badge/Java-19-blue)
[![Gradle version](https://img.shields.io/badge/Gradle-8-green)](https://img.shields.io/badge/Gradle-8-green)
[![Spring Boot Version](https://img.shields.io/badge/Spring%20Boot-3.0.6-green)](https://img.shields.io/badge/Spring%20Boot-3.0.6-green)
[![CI/CD status](https://gitlab.com/vitali-rapalis/test-containers/badges/main/pipeline.svg)](https://gitlab.com/vitali-rapalis/test-containers/-/pipelines)

# Test Containers

---

> Example Project of using [**Testcontainers**](https://www.testcontainers.org/) for Java. This is a Java library that
> supports JUnit tests, providing lightweight, throwaway instances of common databases, Selenium web browsers, or
> anything
> else that can run in a Docker container.

# Getting Started

1. Start application dependencies **postgres database**, by executing **docker compose** file from project root dir.
    ```
    docker compose -f docker/docker-compose.yml up -d
    ```

2. Run application either by **gradle** (spring boot plugin) or **intellij**, below is gradle example.
   ```
   ./gradlew bootRun
   ```

3. Execute **curl** cli command to test live rest endpoints.
   ```
   curl http://localhost:8080/api/cars 
   ```

# Tests

Integration tests are running with the help of [**Testcontainers**](https://www.testcontainers.org/) configured in
this [TestcontainersInitializer](./src/test/java/com/konigsberger/www/testcontainers/domain/car/TestcontainersInitializer.java)
class.
This class will start **postgres** database **docker** container for all tests and then stop after all tests.
To write integration test
this [EnableIntegrationTest](./src/test/java/com/konigsberger/www/testcontainers/domain/car/EnableIntegrationTest.java)
annotation can be used. Below is example of integration test.
```
@EnableIntegrationTest
class YourTest {
   
   @Test
   void yourTestMethod() {
      // ...
   } 
}
```
