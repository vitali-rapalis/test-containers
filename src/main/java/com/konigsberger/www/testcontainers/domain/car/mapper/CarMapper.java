package com.konigsberger.www.testcontainers.domain.car.mapper;

import com.konigsberger.www.testcontainers.domain.car.dto.CarDto;
import com.konigsberger.www.testcontainers.domain.car.entity.CarEntity;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CarMapper {

    CarDto carToCarDto(CarEntity car);

    CarEntity carDtoToCarEntity(CarDto dto);
}
