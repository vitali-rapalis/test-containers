package com.konigsberger.www.testcontainers.domain.car.service;

import com.konigsberger.www.testcontainers.domain.car.dto.CarDto;
import com.konigsberger.www.testcontainers.domain.car.mapper.CarMapper;
import com.konigsberger.www.testcontainers.domain.car.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final CarMapper carMapper;

    @Override
    public ResponseEntity<List<CarDto>> findAll() {
        val cars = carRepository.findAll().stream()
                .map(carMapper::carToCarDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(cars);
    }
}
