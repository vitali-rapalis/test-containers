package com.konigsberger.www.testcontainers.domain.car.dto;

public record CarDto(Integer id, String producer) {
}
