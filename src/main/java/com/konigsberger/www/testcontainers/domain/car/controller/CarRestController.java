package com.konigsberger.www.testcontainers.domain.car.controller;

import com.konigsberger.www.testcontainers.domain.car.dto.CarDto;
import com.konigsberger.www.testcontainers.domain.car.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class CarRestController {
    private final CarService carService;

    @GetMapping("cars")
    public ResponseEntity<List<CarDto>> findAll() {
        return carService.findAll();
    }
}
