package com.konigsberger.www.testcontainers.domain.car.service;

import com.konigsberger.www.testcontainers.domain.car.dto.CarDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CarService {
    ResponseEntity<List<CarDto>> findAll();
}
