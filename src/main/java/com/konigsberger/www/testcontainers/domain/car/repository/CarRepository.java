package com.konigsberger.www.testcontainers.domain.car.repository;

import com.konigsberger.www.testcontainers.domain.car.entity.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<CarEntity, Long> {
}
