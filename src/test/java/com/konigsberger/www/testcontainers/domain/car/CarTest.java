package com.konigsberger.www.testcontainers.domain.car;

import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.List;

@EnableIntegrationTest
@DisplayName("Car domain business cases test")
public class CarTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    @DisplayName("Rest controller get cars should return cars test")
    void restControllerShouldReturnCarsTest() {
        // When
        val response = testRestTemplate.getForEntity("http://localhost:8080/api/cars", List.class);

        // Then
        Assertions.assertThat(response.getBody().size()).isGreaterThan(1);
    }
}
