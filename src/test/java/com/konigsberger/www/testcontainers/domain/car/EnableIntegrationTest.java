package com.konigsberger.www.testcontainers.domain.car;

import com.konigsberger.www.testcontainers.TestContainersApp;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@SpringBootConfiguration
@SpringBootTest(classes = TestContainersApp.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(initializers = TestcontainersInitializer.class)
public @interface EnableIntegrationTest {
}
